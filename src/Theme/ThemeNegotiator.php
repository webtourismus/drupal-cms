<?php

namespace Drupal\wt_cms\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 *  Use the admin theme for layout builder and for authlink editing
 */
class ThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(RequestStack $request_stack, ConfigFactoryInterface $configFactory) {
    $this->requestStack = $request_stack;
    $this->configFactory = $configFactory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $routeName = $route_match->getRouteName();
    if (in_array($routeName, \WT_CMS_LAYOUT_BUILDER_ROUTES) ||
      $this->requestStack->getCurrentRequest()->query->get('authkey') ||
      $routeName == 'entity.user.canonical' ||
      $routeName == 'masquerade.block')
    {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->configFactory->get('system.theme')->get('admin');
  }

}
