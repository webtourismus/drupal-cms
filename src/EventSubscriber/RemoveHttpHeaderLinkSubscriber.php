<?php

namespace Drupal\wt_cms\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Listener for handling Response Header.
 */
class RemoveHttpHeaderLinkSubscriber implements EventSubscriberInterface {
  /**
   * Remove the "link" HTTP header option. This is totally redundant to
   * meta tags in the HTML header and is potentially conflicting if somebody
   * modifies HTML header meta tags and forgets to do the same in HTTP header.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function removeHttpHeaderLink(ResponseEvent $event) {
    $response = $event->getResponse();
    $response->headers->remove('link');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['removeHttpHeaderLink', -10];
    return $events;
  }

}
