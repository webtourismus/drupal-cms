<?php


namespace Drupal\wt_cms\Plugin\Layout;


use Drupal\Core\Form\FormStateInterface;

/**
 * Layout plugin for "repeat"-based grid layout
 */
class RepeatcolLayout extends CmsLayout {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    unset($configuration["columns"]);
    unset($configuration["rows"]);
    $configuration["area_width"] = 300;

    return $configuration;
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    unset($form['section']['columns']);
    unset($form['section']['rows']);

    $form['section'] = [
        'area_width' => [
          '#type' => 'number',
          '#title' => $this->t("Width of columns"),
          '#default_value' => $this->configuration["area_width"],
          '#description' => $this->t("Each block placed in this section will add a column of at least this width. The section will automatically fit and wrap multiple columns."),
          '#min' => 1,
          '#max' => 9999,
          '#step' => 1,
          '#field_suffix' => 'px',
          '#required' => TRUE,
        ],
      ] + $form['section'];

    //this section has only one area. move settings to section and hide the area
    $form['section']['vertical_align'] = $form['main']['vertical_align'];
    unset($form['main']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['area_width'] = $form_state->getValue([
      'section',
      'area_width',
    ]);

    //this is the common config option, but it was moved from $form[$area] to $form['section']
    $this->configuration['main__vertical_align'] = $form_state->getValue([
      'section',
      'vertical_align',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    //the repeatcol layout has only one region, and every block in there should
    //behave like a region. therefore we need to move the rows CSS class
    //from the wrapper to the single "main" region
    /**
     * @var $build['#wrapper_attributes'] \Drupal\Core\Template\Attribute
     */
    $build['#wrapper_attributes']->setAttribute('style', '--repeatcol--width: ' . $this->configuration['area_width'] . 'px;');

    return $build;
  }


  protected function getVerticalAlignOptions() {
    return [
      'top' => $this->t('top'),
      'center' => $this->t('center'),
      'bottom' => $this->t('bottom'),
      'stretch' => $this->t('justified'),
    ];
  }

  protected function getColumnsOptions() {
    return [];
  }


  protected function getRowsOptions() {
    return [];
  }

}
