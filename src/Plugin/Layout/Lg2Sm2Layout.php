<?php


namespace Drupal\wt_cms\Plugin\Layout;


use Drupal\Core\Form\FormStateInterface;

class Lg2Sm2Layout extends CmsLayout {

  protected function getColumnsOptions() {
    return [
      '1-1-1' => '33% - 33% - 33%',
      '4-3-3' => '40% - 30% - 30%',
      '3-4-3' => '30% - 40% - 30%',
      '3-3-4' => '30% - 30% - 40%',
      '2-1-1' => '50% - 25% - 25%',
      '1-2-1' => '25% - 50% - 25%',
      '1-1-2' => '25% - 25% - 50%',
      '3-1-1' => '60% - 20% - 20%',
      '1-3-1' => '20% - 60% - 20%',
      '1-1-3' => '20% - 20% - 60%',
    ];
  }
}
