<?php

namespace Drupal\wt_cms\Plugin\Layout;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Template\Attribute;

/**
 * Base class of layouts for all wt_cms sections.
 */
abstract class CmsLayout extends LayoutDefault implements PluginFormInterface {

  const BACKGROUND_REGION = 'background';

  /**
   * Some child classes need explicit responsive settings
   */
  protected function getBreakpoints() {
    return [
      'xs' => $this->t('mobile phone'),
      'sm' => $this->t('tablet portrait'),
      'md' => $this->t('tablet landscape'),
      'lg' => $this->t('PC'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = LayoutDefault::defaultConfiguration();

    $configuration["columns"] = array_key_first($this->getColumnsOptions());
    $configuration["rows"] = array_key_first($this->getRowsOptions());
    $configuration['section_width'] = array_key_first($this->getSectionWidthOptions());
    $configuration['color_set'] = array_key_first($this->getColorsetOptions());
    $configuration['top_padding'] = array_key_exists('2rem', $this->getVerticalPaddingOptions()) ? '2rem' : array_key_first($this->getVerticalPaddingOptions());
    $configuration['bottom_padding'] = array_key_exists('2rem', $this->getVerticalPaddingOptions()) ? '2rem' : array_key_first($this->getVerticalPaddingOptions());
    $configuration['inner_gap'] = array_key_exists('small', $this->getInnerGapOptions()) ? 'small' : array_key_first($this->getInnerGapOptions());

    $region_defs = $this->getPluginDefinition()->getRegions();
    foreach ($region_defs as $key => $region_def) {
      if ($key !== self::BACKGROUND_REGION) {
        $configuration["{$key}__vertical_align"] = array_key_first($this->getVerticalAlignOptions());
      }
    }

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['section'] = [
      '#type' => 'details',
      '#title' => $this->t('Section'),
      '#tree' => TRUE,
    ];
    $form['section']['columns'] = [
      '#type' => 'select',
      '#title' => $this->t('Columns'),
      '#default_value' => $this->configuration["columns"] ?? NULL,
      '#options' => $this->getColumnsOptions(),
      '#required' => TRUE,
    ];
    $form['section']['rows'] = [
      '#type' => 'select',
      '#title' => $this->t('Rows'),
      '#default_value' => $this->configuration["rows"] ?? NULL,
      '#options' => $this->getRowsOptions(),
      '#required' => TRUE,
    ];
    $form['section']['section_width'] = [
      '#type' => 'select',
      '#title' => $this->t('Section width'),
      '#default_value' => $this->configuration['section_width'] ?? NULL,
      '#options' => $this->getSectionWidthOptions(),
      '#required' => TRUE,
    ];
    $form['section']['color_set'] = [
      '#type' => 'select',
      '#title' => $this->t('Color schema'),
      '#default_value' => $this->configuration['color_set'] ?? NULL,
      '#options' => $this->getColorsetOptions(),
      '#required' => FALSE,
    ];
    $form['section']['top_padding'] = [
      '#type' => 'select',
      '#title' => $this->t('Top padding'),
      '#default_value' => $this->configuration['top_padding'] ?? NULL,
      '#options' => $this->getVerticalPaddingOptions(),
      '#required' => TRUE,
    ];
    $form['section']['bottom_padding'] = [
      '#type' => 'select',
      '#title' => $this->t('Bottom padding'),
      '#default_value' => $this->configuration['bottom_padding'] ?? NULL,
      '#options' => $this->getVerticalPaddingOptions(),
      '#required' => TRUE,
    ];
    $form['section']['inner_gap'] = [
      '#type' => 'select',
      '#title' => $this->t('Inner padding'),
      '#default_value' => $this->configuration['inner_gap'] ?? NULL,
      '#options' => $this->getInnerGapOptions(),
      '#required' => TRUE,
    ];

    $region_defs = $this->getPluginDefinition()->getRegions();
    foreach ($region_defs as $key => $region_def) {
      if ($key !== self::BACKGROUND_REGION) {
        $form[$key] = [
          '#type' => 'details',
          '#title' => $region_def['label'],
          '#tree' => TRUE,
        ];

        $form[$key]['vertical_align'] = [
          '#type' => 'select',
          '#title' => $this->t('Vertical alignment'),
          '#default_value' => $this->configuration["{$key}__vertical_align"] ?? NULL,
          '#options' => $this->getVerticalAlignOptions(),
          '#required' => TRUE,
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if ($form_state->hasValue(['section', 'columns'])) {
      $this->configuration['columns'] = $form_state->getValue([
        'section',
        'columns',
      ]);
    }

    if ($form_state->hasValue(['section', 'rows'])) {
      $this->configuration['rows'] = $form_state->getValue([
        'section',
        'rows',
      ]);
    }

    if ($form_state->hasValue(['section', 'section_width'])) {
      $this->configuration['section_width'] = $form_state->getValue([
        'section',
        'section_width',
      ]);
    }
    if ($form_state->hasValue(['section', 'color_set'])) {
      $this->configuration['color_set'] = $form_state->getValue([
        'section',
        'color_set',
      ]);
    }
    if ($form_state->hasValue(['section', 'top_padding'])) {
      $this->configuration['top_padding'] = $form_state->getValue([
        'section',
        'top_padding',
      ]);
    }
    if ($form_state->hasValue(['section', 'bottom_padding'])) {
      $this->configuration['bottom_padding'] = $form_state->getValue([
        'section',
        'bottom_padding',
      ]);
    }
    if ($form_state->hasValue(['section', 'inner_gap'])) {
      $this->configuration['inner_gap'] = $form_state->getValue([
        'section',
        'inner_gap',
      ]);
    }

    $region_defs = $this->getPluginDefinition()->getRegions();
    foreach ($region_defs as $key => $region_def) {
      if ($key !== self::BACKGROUND_REGION) {
        if ($form_state->hasValue([$key, 'vertical_align'])) {
          $this->configuration["{$key}__vertical_align"] = $form_state->getValue([
            $key,
            "vertical_align",
          ]);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    $region_defs = $this->getPluginDefinition()->getRegions();
    foreach ($region_defs as $key => $region_def) {
      if (!array_key_exists($key, $regions)) {
        $build[$key] = [];
      }
    }

    $section_id = $this->getPluginDefinition()->id();
    $section_label = $this->getPluginDefinition()->getLabel();
    $section_group = $this->getPluginDefinition()->getTemplate();
    $build['#section'] = [
      '#id' => $section_id,
      '#label' => $section_label,
      '#group' => $section_group,
    ];

    $build['#attributes']['id'] = Html::getUniqueId($this->configuration['label'] ?: 'section');
    $build['#attributes']['class'] = [
      'section',
      "section--{$section_group}",
      "section--{$section_id}",
    ];

    if ($this->configuration['section_width'] ?? NULL) {
      $build['#attributes']['class'][] = 'section--' . $this->configuration['section_width'];
    }
    if ($this->configuration['top_padding'] ?? NULL) {
      $build['#attributes']['class'][] = 'pt-' . $this->configuration['top_padding'];
    }
    if ($this->configuration['bottom_padding'] ?? NULL) {
      $build['#attributes']['class'][] = 'pb-' . $this->configuration['bottom_padding'];
    }
    if ($this->configuration['color_set'] ?? NULL) {
      $build['#attributes']['class'][] = 'colorscheme';
      $build['#attributes']['class'][] = 'colorscheme--' . $this->configuration['color_set'];
    }

    $build['#wrapper_attributes'] = new Attribute(['class' => []]);
    $build['#wrapper_attributes']['class'][] = 'wrapper';
    if ($this->configuration['section_width'] ?? NULL) {
      $build['#wrapper_attributes']['class'][] = 'wrapper--' . $this->configuration['section_width'];
    }
    $build['#wrapper_attributes']['class'][] = "wrapper--{$section_group}";
    $build['#wrapper_attributes']['class'][] = "wrapper--{$section_id}";

    if ($this->configuration["rows"] ?? NULL) {
      $build['#wrapper_attributes']['class'][] = 'wrapper--rows-' . $this->configuration['rows'];
    }

    if ($this->configuration["columns"] ?? NULL) {
      $build['#wrapper_attributes']['class'][] = 'wrapper--cols-' . $this->configuration['columns'];
    }

    if ($this->configuration["inner_gap"] ?? NULL) {
      $build['#wrapper_attributes']['class'][] = 'wrapper--gap-' . $this->configuration['inner_gap'];
    }

    foreach ($region_defs as $key => $region_def) {
      if (array_key_exists($key, $build)) {
        $build['#section'][$key] = isset($region_def['label']) ? $region_def['label']->render() : '';
        if (!array_key_exists('#attributes', $build[$key])) {
          $build[$key]['#attributes'] = new Attribute(['class' => []]);
        }
        $build[$key]['#attributes']['class'][] = 'area';
        $build[$key]['#attributes']['class'][] = "area--{$section_group}";
        $build[$key]['#attributes']['class'][] = "area--{$section_id}";
        $build[$key]['#attributes']['class'][] = "area--{$section_group}--{$key}";
        $build[$key]['#attributes']['class'][] = "area--{$section_id}--{$key}";

        if ($key === self::BACKGROUND_REGION) {
          $build[$key]['#attributes']['class'][] = 'area--background';
          if (!strpos($this->configuration['color_set'], '-over-image')) {
            $build[$key]['#attributes']['class'][] = 'hidden';
          }
        }
        else {
          if ($this->configuration['inner_gap'] ?? NULL) {
            $build[$key]['#attributes']['class'][] = 'area--gap-' . $this->configuration['inner_gap'];
          }
          if ($this->configuration["{$key}__vertical_align"] ?? NULL) {
            $build[$key]['#attributes']['class'][] = 'area--vertical-align-' . $this->configuration["{$key}__vertical_align"];
          }
        }

      }
    }

    return $build;
  }

  protected function getSectionWidthOptions() {
    return [
      'limited' => $this->t('side padding & limited width'),
      'padded' => $this->t('side padding & full width'),
      'seamless' => $this->t('no padding & full width'),
    ];
  }

  protected function getColorsetOptions() {
    return [
      'dark-over-light' => $this->t('dark text over default background'),
      'dark-over-gray' => $this->t('dark text over accented background'),
      'light-over-dark' => $this->t('light text over dark background'),
      'inverse-over-garish' => $this->t('inverse text over garish background'),
      'dark-over-image' => $this->t('dark text over image background'),
      'light-over-image' => $this->t('light text over image background'),
    ];
  }

  protected function getVerticalAlignOptions() {
    return [
      'top' => $this->t('top'),
      'center' => $this->t('center'),
      'bottom' => $this->t('bottom'),
      'stretch' => $this->t('justified'),
      'space-between' => $this->t('space between'),
      'space-around' => $this->t('space around'),
    ];
  }

  protected function getRowsOptions() {
    return [
      'min-min' => $this->t('growing by content'),
      '1-1' => $this->t('force same size'),
      'min-1' => $this->t('upper min, lower growing'),
      '1-min' => $this->t('upper growing, lower min'),
    ];
  }

  abstract protected function getColumnsOptions();

  public static function getInnerGapOptions() {
    return [
      'none' => t('none'),
      'thin' => t('thin line'),
      'small' => t('weakly growing'),
      'large' => t('strongly growing'),
    ];
  }

  //public static because is used as callback for allowed field values
  public static function getVerticalPaddingOptions() {
    return array_merge(self::getInnerGapOptions(),
      [
        '1rem' => t('1 text line'),
        '2rem' => t('2 text lines'),
        '3rem' => t('3 text lines'),
        '4rem' => t('4 text lines'),
        '5rem' => t('5 text lines'),
        '6rem' => t('6 text lines'),
        '7rem' => t('7 text lines'),
        '8rem' => t('8 text lines'),
        '9rem' => t('9 text lines'),
      ]);
  }

}
