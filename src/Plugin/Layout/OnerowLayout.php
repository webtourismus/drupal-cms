<?php

namespace Drupal\wt_cms\Plugin\Layout;


use Drupal\Core\Form\FormStateInterface;

/**
 * Layout plugin for all flex-based Onerow layouts
 */
class OnerowLayout extends CmsLayout {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    unset($configuration["columns"]);
    unset($configuration["rows"]);
    $configuration["justify_content"] = array_key_first($this->getJustifyContentOptions());

    $region_defs = $this->getPluginDefinition()->getRegions();
    $breakpoints = $this->getBreakpoints();
    $defaultvalue_xs = '1-1';
    $defaultvalue_sm = '1-' . (count($region_defs) - 1);
    $defaultvalue_md_up = '';
    foreach ($region_defs as $key => $region_def) {
      if ($key !== self::BACKGROUND_REGION) {
        $default_value = $defaultvalue_xs;
        $prev = FALSE;
        foreach ($breakpoints as $bp => $breakpoint_label) {
          $configuration["{$key}__width_{$bp}"] = $default_value;
          $prev = $bp;
          $default_value = ($prev === array_key_first($breakpoints) ? $defaultvalue_sm : $defaultvalue_md_up);
        }
      }
    }

    return $configuration;
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    unset($form['section']['rows']);
    unset($form['section']['columns']);
    $form['section'] = [
        'justify_content' => [
          '#type' => 'select',
          '#title' => $this->t("Horizontal alignment"),
          '#default_value' => $this->configuration["justify_content"],
          '#options' => $this->getJustifyContentOptions(),
          '#required' => TRUE,
        ],
      ] + $form['section'];

    $region_defs = $this->getPluginDefinition()->getRegions();
    $breakpoints = $this->getBreakpoints();
    foreach ($region_defs as $key => $region_def) {
      if ($key !== self::BACKGROUND_REGION) {
        $prev = FALSE;
        $flexOptionFormElements = [];
        foreach ($breakpoints as $bp => $breakpoint_label) {
          $flexOptionFormElements["width_{$bp}"] = [
            '#type' => 'select',
            '#title' => $this->t("Width on {$breakpoint_label}"),
            '#default_value' => $this->configuration["{$key}__width_{$bp}"],
            '#options' => $this->getFlexOptions(),
            '#empty_option' => ($prev === FALSE ? NULL : $this->t('like @device', ['@device' => $breakpoints[$prev]])),
            '#empty_value' => ($prev === FALSE ? NULL : ''),
            '#required' => ($bp === array_key_first($breakpoints)),
          ];
          $prev = $bp;
        }
        $form[$key] = $flexOptionFormElements + $form[$key];
      }
    }

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['justify_content'] = $form_state->getValue([
      'section',
      'justify_content',
    ]);
    $region_defs = $this->getPluginDefinition()->getRegions();
    foreach ($region_defs as $key => $region_def) {
      if ($key !== self::BACKGROUND_REGION) {
        foreach ($this->getBreakpoints() as $bp => $breakpoint_label) {
          $this->configuration["{$key}__width_{$bp}"] = $form_state->getValue([
            $key,
            "width_{$bp}",
          ]);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    $build['#wrapper_attributes']['class'][] = 'wrapper--onerow-' . $this->configuration['justify_content'];
    $bottomSpacing = $this->configuration['bottom_padding'];
    $build['#attributes']['class'] = array_diff($build['#attributes']['class'], ['pb-' . $bottomSpacing]);

    $region_defs = $this->getPluginDefinition()->getRegions();
    foreach ($region_defs as $key => $region_def) {
      if ($key !== self::BACKGROUND_REGION) {
        $build[$key]['#attributes']['class'][] = 'mb-' . $bottomSpacing;
        foreach ($this->getBreakpoints() as $bp => $breakpoint_label) {
          if ($this->configuration["{$key}__width_{$bp}"] == 'hidden') {
            $build[$key]['#attributes']['class'][] = "hidden@{$bp}-up";
          }
          else {
            $build[$key]['#attributes']['class'][] = 'area--onerow-' . $this->configuration["{$key}__width_{$bp}"] . "@{$bp}-up";
          }
        }
      }
    }
    return $build;
  }


  protected function getFlexOptions() {
    return [
      'divide' => $this->t('divide'),
      '1-1' => '100%',
      '5-6' => '83.3%',
      '4-5' => '80%',
      '3-4' => '75%',
      '2-3' => '66.7%',
      '3-5' => '60%',
      '1-2' => '50%',
      '2-5' => '40%',
      '1-3' => '33.3%',
      '3-10' => '30%',
      '1-4' => '25%',
      '1-5' => '20%',
      '1-6' => '16.7%',
      '1-10' => '10%',
      '1-12' => '8.3%',
      '1-20' => '5%',
      'shrink' => $this->t('as narrow as possible'),
      'hidden' => $this->t('hidden'),
    ];
  }

  protected function getJustifyContentOptions() {
    return [
      'flex-start' => $this->t('left'),
      'center' => $this->t('center'),
      'flex-end' => $this->t('right'),
      'space-between' => $this->t('space between'),
      'space-around' => $this->t('space around'),
      'space-evenly' => $this->t('space evenly'),
    ];
  }

  protected function getColumnsOptions() {
    return [];
  }

}
