<?php


namespace Drupal\wt_cms\Plugin\Layout;


use Drupal\Core\Form\FormStateInterface;

class Lg1Sm2Layout extends CmsLayout {

  protected function getColumnsOptions() {
    return [
      '1-1' => '50% - 50%',
      '1-2' => '33% - 67&',
      '2-1' => '67% - 33%',
      '2-3' => '40% - 60%',
      '3-2' => '60% - 40%',
      '1-4' => '20% - 80%',
      '4-1' => '80% - 20%',
    ];
  }
}
