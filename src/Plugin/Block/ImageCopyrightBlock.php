<?php

namespace Drupal\wt_cms\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plain text list of image copyright stored in "title" column of image fields
 *
 * @Block(
 *   id = "wt_image_copyright",
 *   admin_label = @Translation("Images copyright"),
 * )
 */
class ImageCopyrightBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected Connection $database;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $tables = array_filter(explode("\n", $config['tables']));
    $entityTypes = [];
    $subqueries = [];
    foreach ($tables as $table) {
      $table = trim($table);
      list($entityTypes[], $field) = explode('__', $table);
      $subqueries[] = 'SELECT TRIM('. $field . '_title) AS title FROM {' . $table . '} WHERE ' . $field . '_title IS NOT NULL';
    }
    $subquery = join(' UNION ALL ', $subqueries);
    $results = $this->database->query('SELECT DISTINCT title FROM (' . $subquery . ') AS distinct_summary WHERE title <> \'\'')->fetchCol();
    $resultString = join($config['separator'], $results);

    $built = [
      '#type' => 'markup',
      '#markup' => '<p class="copyright__data">' . $resultString . '</p>',
    ];

    return $built;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $config = $this->getConfiguration();
    $tables = array_filter(explode("\n", $config['tables']));
    $entityTypes = [];
    $subqueries = [];
    foreach ($tables as $table) {
      $table = trim($table);
      list($entityTypes[], $field) = explode('__', $table);
    }
    $cacheKeys = array_map(function($entityType) {return $entityType . '_list';}, $entityTypes);
    return Cache::mergeTags(parent::getCacheTags(), $cacheKeys);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['tables'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tables'),
      '#required' => TRUE,
      '#default_value' => isset($config['tables']) ? $config['tables'] : 'media__field_media_image',
      '#description' => $this->t('One database table per line. Each table must be an image field with copyright information stored in the "title" column.')
    ];

    $form['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Separator'),
      '#required' => TRUE,
      '#default_value' => isset($config['separator']) ? $config['separator'] : ', ',
      '#description' => $this->t('Separator in between copyright entries. HTML allowed.')
    ];

    return $form;
  }

  public function blockValidate($form, FormStateInterface $form_state) {
    $tableString = $form_state->getValue('tables');

    $tables = array_filter(explode("\n", $tableString));
    foreach ($tables as $table) {
      $table = trim($table);
      if(preg_match('/[^a-z_\0-9]/i', $table)) {
        $form_state->setErrorByName('tables', $this->t('Invalid table name "@name", only a-z, 0-9 and underscore allowed', ['@name' => $table]));
      }
      list($entityType, $field) = explode('__', $table);
      if (!$entityType) {
        $form_state->setErrorByName('tables', $this->t('Can\'t detect base entity type in table "@name"', ['@name' => $table]));
      }
      if (!$field) {
        $form_state->setErrorByName('tables', $this->t('Can\'t detect field name in table "@name"', ['@name' => $table]));
      }
      try {
        $this->database->query('SELECT '. $field . '_title FROM {' . $table . '} LIMIT 1')->fetchCol();
      }
      catch (DatabaseException $e) {
        $form_state->setErrorByName('tables', $this->t('Could not query column "@name"', ['@name' => "{$table}.{$field}_title"]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['tables'] = $values['tables'];
    $this->configuration['separator'] = $values['separator'];
  }
}
