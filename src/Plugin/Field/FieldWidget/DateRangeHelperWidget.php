<?php

namespace Drupal\wt_cms\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'daterange_helper' widget.
 *
 * @FieldWidget(
 *   id = "daterange_helper",
 *   label = @Translation("Date and time range with helpers to set end date"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangeHelperWidget extends DateRangeDefaultWidget implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['value']['helpers_sameday'] = [
      '#type' => 'item',
      '#markup' => '<button type="button" class="datetime__helper--sameday">' . $this->t('ends same day') . '</button>'
    ];

    // Identify the type of date and time elements to use.
    switch ($this->getFieldSetting('datetime_type')) {
      case DateRangeItem::DATETIME_TYPE_DATE:
      case DateRangeItem::DATETIME_TYPE_ALLDAY:
        break;
      default:
        $element['value']['helpers_hour'] = [
          '#type' => 'item',
          '#markup' => $this->t('ends <button type="button" class="datetime__helper--hours">1</button> <button type="button" class="datetime__helper--hours">2</button> <button type="button" class="datetime__helper--hours">3</button> hours later'),
        ];
        break;
    }
    $element['#attached']['library'][] = 'wt_cms/daterange_helper';
    return $element;
  }

}
