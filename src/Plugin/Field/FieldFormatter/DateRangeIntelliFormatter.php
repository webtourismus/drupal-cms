<?php

namespace Drupal\wt_cms\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeCustomFormatter;
use Drupal\datetime_range\DateTimeRangeTrait;

/**
 * Plugin implementation of the 'Intelli' formatter for 'daterange' fields.
 *
 * This formatter renders the data range as plain text, with a fully
 * configurable date format using the PHP date syntax and separator.
 * Furthermore, parts of the end date are omitted if they are not indiffernet
 * to the start date.
 *
 * @FieldFormatter(
 *   id = "daterange_intelli",
 *   label = @Translation("Intelligent shortened"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangeIntelliFormatter extends DateRangeCustomFormatter {

  use DateTimeRangeTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'time_format' => 'H:i',
        'time_separator' => ' - ',
        'datetime_separator' => ', ',
        'hide_0000' => 1,
        'hide_2359' => 1,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    foreach ($items as $delta => $item) {
      if (!empty($item->start_date) && !empty($item->end_date)) {
        $elements[$delta] = $this->buildDateByString($this->formatDaterange($item->start_date, $item->end_date));
      }
      else {
        $elements[$delta] = $this->buildDateByString($this->formatStartdate($item->start_date));
      }
    }
    return $elements;
  }

  protected function buildDateByString(string $formattedDate) {
    $build = [
      '#markup' => $formattedDate,
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
    return $build;
  }

  protected function formatStartdate(DrupalDateTime $date) {
    $dateformat = $this->getSetting('date_format');
    $timeformat = $this->getSetting('time_format');
    $timezone = $this->getSetting('timezone_override') ?: $date->getTimezone()
      ->getName();

    $result = $this->dateFormatter->format($date->getTimestamp(), 'custom', $dateformat, $timezone != '' ? $timezone : NULL);

    $hideTime = FALSE;
    if ($this->getSetting('hide_0000')) {
      $hideTime = $this->dateFormatter->format($date->getTimestamp(), 'custom', 'H:i', $timezone != '' ? $timezone : NULL) == '00:00';
    }
    if (!$hideTime) {
      $result .= $this->getSetting('datetime_separator') . $this->dateFormatter->format($date->getTimestamp(), 'custom', $timeformat, $timezone != '' ? $timezone : NULL);
    }

    return $result;
  }

  protected function formatDaterange(DrupalDateTime $startDate, DrupalDateTime $endDate) {
    $dateformat = $this->getSetting('date_format');
    $timeformat = $this->getSetting('time_format');
    $timezone = $this->getSetting('timezone_override') ?: $endDate->getTimezone()
      ->getName();

    $result = $this->dateFormatter->format($startDate->getTimestamp(), 'custom', $dateformat, $timezone != '' ? $timezone : NULL);

    if ($startDate->format('d.m.Y') != $endDate->format('d.m.Y')) {
      $result .= $this->getSetting('separator');
      $result .= $this->dateFormatter->format($endDate->getTimestamp(), 'custom', $dateformat, $timezone != '' ? $timezone : NULL);
    }

    $hideDate = $this->dateFormatter->format($endDate->getTimestamp(), 'custom', $dateformat, $timezone != '' ? $timezone : NULL) == $this->dateFormatter->format($startDate->getTimestamp(), 'custom', $dateformat, $timezone != '' ? $timezone : NULL);

    $hideStartTime = FALSE;
    if ($this->getSetting('hide_0000')) {
      $hideStartTime = $this->dateFormatter->format($startDate->getTimestamp(), 'custom', 'H:i', $timezone != '' ? $timezone : NULL) == '00:00';
    }
    if (!$hideStartTime) {
      $result .= $this->getSetting('datetime_separator');
      $result .= $this->dateFormatter->format($startDate->getTimestamp(), 'custom', $timeformat, $timezone != '' ? $timezone : NULL);
    }

    $hideEndTime = FALSE;
    if ($this->getSetting('hide_2359')) {
      $hideEndTime = $this->dateFormatter->format($endDate->getTimestamp(), 'custom', 'H:i', $timezone != '' ? $timezone : NULL) == '23:59';
    }
    if (!$hideEndTime) {
      if ($hideStartTime) {
        $result .= $this->getSetting('datetime_separator');
      }
      else {
        $result .= $this->getSetting('time_separator');
      }
      $result .= $this->dateFormatter->format($endDate->getTimestamp(), 'custom', $timeformat, $timezone != '' ? $timezone : NULL);
    }

    return $result;
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['date_format']['#title'] = $this->t('Date format');

    $form['time_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time format'),
      '#description' => $this->t('See <a href="http://php.net/manual/function.date.php" target="_blank">the documentation for PHP date formats</a>.'),
      '#default_value' => $this->getSetting('time_format'),
    ];

    $form['time_separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time separator'),
      '#description' => $this->t('The string to separate the start and end time'),
      '#default_value' => $this->getSetting('time_separator'),
    ];

    $form['datetime_separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date and time separator'),
      '#description' => $this->t('The string to separate the day and the time'),
      '#default_value' => $this->getSetting('datetime_separator'),
    ];

    $form['hide_0000'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide start time when time is 00:00'),
      '#default_value' => $this->getSetting('hide_0000'),
    ];

    $form['hide_2359'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide end time when time is 23:59'),
      '#default_value' => $this->getSetting('hide_2359'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $date = new DrupalDateTime();
    $this->setTimeZone($date);
    array_pop($summary);
    $summary[] = $date->format($this->getSetting('time_format'), $this->getFormatSettings());
    if ($this->getSetting('hide_0000')) {
      $summary[] = $this->t('Hide start time when 00:00');
    }
    if ($this->getSetting('hide_2359')) {
      $summary[] = $this->t('Hide end time when 23:59');
    }

    return $summary;
  }

}
