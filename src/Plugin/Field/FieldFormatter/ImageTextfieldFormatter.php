<?php

namespace Drupal\wt_cms\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'image_textfield' formatter.
 *
 * @FieldFormatter(
 *   id = "image_textfield",
 *   label = @Translation("Image text field"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageTextfieldFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'subfield' => 'alt',
    ];
  }

  protected function getSubFields() {
    return [
      'alt' => $this->t('Image alt text'),
      'title' => $this->t('Image title text')
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['subfield'] = [
      '#title' => t('Text field'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('subfield'),
      '#options' => $this->getSubFields(),
      '#required' => true,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [$this->getSubFields()[$this->getSetting('image_link')]];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $subfield = $this->getSetting('subfield');
    /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $image */
    foreach ($items as $delta => $image) {
      $elements[$delta] = [
          '#type' => 'markup',
          '#markup' => $image->get('title')->getValue()
        ];
    }
    return $elements;
  }

}
