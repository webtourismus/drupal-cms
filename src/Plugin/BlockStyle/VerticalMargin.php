<?php

namespace Drupal\wt_cms\Plugin\BlockStyle;

use Drupal\block_style_plugins\Plugin\BlockStyleBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wt_cms\Plugin\Layout\CmsLayout;

/**
 * Provides a 'VerticalMargin' block style to add "mt-*" margin-top
 * and "mb-*" margin-bottom utility CSS classes
 *
 * @BlockStyle(
 *  id = "vertical_margin",
 *  label = @Translation("Vertical margin"),
 * )
 */
class VerticalMargin extends BlockStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['margin_top' => '', 'margin_bottom' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $vertical_margins = CmsLayout::getInnerGapOptions() + CmsLayout::getVerticalPaddingOptions();
    $top_margins = [];
    $bottom_margins = [];
    foreach ($vertical_margins as $key => $value) {
      $top_margins['mt-' . $key] = $value;
      $bottom_margins['mb-' . $key] = $value;
    }
    $elements['margin_top'] = [
      '#type' => 'select',
      '#title' => $this->t('Top margin'),
      '#options' => $top_margins,
      '#default_value' => $this->configuration['margin_top'],
    ];
    $elements['margin_bottom'] = [
      '#type' => 'select',
      '#title' => $this->t('Bottom margin'),
      '#options' => $bottom_margins,
      '#default_value' => $this->configuration['margin_bottom'],
    ];

    return $elements;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    /**
     * don't save the "none" value
     */
    if ($form_state->getValue('margin_top') == 'mt-none') {
      $form_state->unsetValue('margin_top');
    }
    if ($form_state->getValue('margin_bottom') == 'mb-none') {
      $form_state->unsetValue('margin_bottom');
    }
    parent::submitConfigurationForm($form, $form_state);
  }

}
