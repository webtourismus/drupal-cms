<?php

namespace Drupal\wt_cms;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\image\Entity\ImageStyle;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Twig extension with some useful functions and filters.
 */
class TwigExtension extends AbstractExtension {

  protected FileUrlGeneratorInterface $fileUrlGenerator;
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs the TwigTweakExtension object.
   */
  public function __construct(FileUrlGeneratorInterface $fileUrlGenerator, EntityTypeManagerInterface $entityTypeManager) {
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->entityTypeManager = $entityTypeManager;
  }

  public function getFilters(): array {
    $filters = [
      new TwigFilter('image_entity_style', [$this, 'imageEntityStyle']),
    ];
    return $filters;
  }

  /**
   * Return an image style URL.
   *
   * This is similar to TwigTweaks image_style filter, but also allows
   * file IDs and file UUIDs as source
   */
  public function imageEntityStyle(string $selector, string $style): ?string {

    if (!$image_style = ImageStyle::load($style)) {
      trigger_error(sprintf('Could not load image style %s.', $style));
      return NULL;
    }

    // Determine selector type by its value.
    if (preg_match('/^\d+$/', $selector)) {
      $selector_type = 'fid';
    }
    elseif (Uuid::isValid($selector)) {
      $selector_type = 'uuid';
    }
    else {
      $selector_type = 'uri';
    }

    $files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties([$selector_type => $selector]);

    if (count($files) < 1) {
      return NULL;
    }

    /** @var $file \Drupal\file\Entity\File */
    $file = reset($files);
    $path = $file->getFileUri();
    if (!$path) {
      return NULL;
    }

    return $this->fileUrlGenerator->generateString($image_style->buildUrl($path));
  }
}
