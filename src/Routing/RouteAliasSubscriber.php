<?php


namespace Drupal\wt_cms\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * This module simply aliases several existing Drupal routes under known, stable
 * paths. Used to create clones with better admin paths and better local tasks
 * for existing Drupal admin stuff, like an alternative media library
 */
class RouteAliasSubscriber extends RouteSubscriberBase {

  const ROUTE_ALIASES = [
    'wt_cms.admin.media-library' => [
      'new_path' => '/admin/media-library',
      'old_route' => 'view.media_library.page',
    ],
    'wt_cms.admin.media-library.table' => [
      'new_path' => '/admin/media-library/table',
      'old_route' => 'view.media.media_page_list',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach (self::ROUTE_ALIASES as $new_route_id => $route_details) {
      $old_route_id = $route_details['old_route'];
      $new_route_path = $route_details['new_path'];

      $old_route = $collection->get($old_route_id);

      if (!empty($old_route)) {
        $new_route = clone($old_route);
        $new_route->setPath($new_route_path);

        $collection->add($new_route_id, $new_route);
      }
    }
  }
}
