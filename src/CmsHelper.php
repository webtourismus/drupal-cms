<?php

namespace Drupal\wt_cms;

use \Drupal\Core\Entity\EntityTypeManagerInterface;
use \Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;

class CmsHelper {

  /**
   * @var $entityTypeManager \Drupal\Core\Entity\EntityTypeManagerInterface;
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * A cache tag that is invalidated by cron every 60 minutes
   */
  public const CACHETAG_HOUR = 'wt_cms_hour';

  /**
   * A cache tag that is invalidated by cron every 24 hours
   */
  public const CACHETAG_DAY = 'wt_cms_day';

  /**
   * A cache tag that is invalidated by cron at the start of every day
   */
  public const CACHETAG_MIDNIGHT = 'wt_cms_midnight';

  /**
   * A cache tag that is invalidated by cron every 7 days
   */
  public const CACHETAG_WEEK = 'wt_cms_week';

  /**
   * A cache tag that is invalidated by cron at the start of every week
   */
  public const CACHETAG_MONDAY = 'wt_cms_monday';

  /**
   * Whenever this viewmode is used anywhere, there somewhere is a dynamic
   * modification of the viewmode in a theme or module file.
   * This is commonly used for media or paragraphs that change their design/size
   * depending on the parent container or another editable input field
   */
  public const VARIABLE_VIEWMODE = 'wt_viewmode_placeholder';

  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match) {
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
    );
  }

  public function getNodeFromRoute() {
    $node = NULL;

    if ($this->routeMatch->getRouteName() == 'entity.node.preview') {
      $node = $this->routeMatch->getParameter('node_preview');
    }
    elseif ($this->routeMatch->getParameter('node')) {
      $node = $this->routeMatch->getParameter('node');
    }

    if (is_numeric($node)) {
      $node = Node::load($node);
    }

    return $node;
  }

  public function getViewIdsFromRoute() {
    $view_id = $this->routeMatch->getParameter('view_id');
    if (!$view_id) {
      return NULL;
    }

    $display_id = $this->routeMatch->getParameter('display_id');
    $display_plugin = $this->routeMatch->getRouteObject()->getOption('_view_display_plugin_id');
    return [
      'views_id' => $view_id,
      'display_id' => $display_id,
      'display_plugin' => $display_plugin,
    ];
  }

}
