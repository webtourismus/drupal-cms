<?php

namespace Drupal\wt_cms;

use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

class TrustedCallbacks implements TrustedCallbackInterface {

  public static function trustedCallbacks() {
    return ['layout_builder_prerender_add_background_button'];
  }

  public static function layout_builder_prerender_add_background_button(array $element) {
    $lb = $element['layout_builder'];

    foreach (Element::children($lb) as $section) {
      if (isset($lb[$section]['layout-builder__section'])) {
        foreach (Element::children($lb[$section]['layout-builder__section']) as $region) {
          if ($region == 'background') {
            /** @var $oldMarkup \Drupal\Core\StringTranslation\TranslatableMarkup */
            $oldMarkup = $element['layout_builder'][$section]['layout-builder__section'][$region]['layout_builder_add_block']['link']['#title'];
            if ($oldMarkup instanceof TranslatableMarkup && $oldMarkup->getUntranslatedString() == 'Add block <span class="visually-hidden">in @section, @region region</span>') {
              $newMarkup = t('Add background <span class="visually-hidden">in @section, @region region</span>', [
                '@section' => $oldMarkup->getArguments()['@section'],
                '@region' => $oldMarkup->getArguments()['@region'],
              ]);
              $element['layout_builder'][$section]['layout-builder__section'][$region]['layout_builder_add_block']['link']['#title'] = $newMarkup;
            }
          }
        }
      }
    }

    return $element;
  }

}
