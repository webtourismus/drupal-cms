<?php

namespace Drupal\wt_cms\Controller;

use Drupal\Core\Access\AccessManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\system\SystemManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for wt_cms routes.
 */
class AdminController extends ControllerBase {

  /**
   * The active menu trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * System Manager Service.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * Access Manager service
   *
   * @var \Drupal\Core\Access\AccessManager
   */
  protected $accessManager;

  public function __construct(MenuActiveTrailInterface $menu_active_trail, SystemManager $system_manager, AccessManager $access_manager) {
    $this->menuActiveTrail = $menu_active_trail;
    $this->systemManager = $system_manager;
    $this->accessManager = $access_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('menu.active_trail'),
      $container->get('system.manager'),
      $container->get('access_manager')
    );
  }

  /**
   * @see \Drupal\system\Controller\SystemController::systemAdminMenuBlockPage()
   */
  public function toolbarMenuBlockPage($menu_name = 'cms') {
    $link = $this->menuActiveTrail->getActiveLink($menu_name);
    if ($link && $content = $this->systemManager->getAdminBlock($link)) {
      $output = [
        '#theme' => 'admin_block_content',
        '#content' => $content,
      ];
    }
    else {
      $output = [
        '#markup' => t('You do not have any administrative items.'),
      ];
    }
    return $output;

  }

  /**
   * Redirects to the first route the current user has access to.
   */
  public function redirectToFirstAllowedRoute(Request $request, $route_names) {
    if (!is_array($route_names)) {
      $routeNames = [$route_names];
    }
    foreach ($route_names as $routeName) {
      if ($this->accessManager->checkNamedRoute($routeName, [], $this->currentUser())) {
        return $this->redirect($routeName);
        break;
      }
    }
  }
}
