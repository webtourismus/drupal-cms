<?php

namespace Drupal\wt_cms\CacheContext;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SeasonCacheContext.
 */
class SeasonCacheContext implements CacheContextInterface {

  /**
   * Only entities owning a field with this name are depending on the
   * season cache-context
   *
   * @see wt_cms_entity_build_defaults_alter()
   */
  public const FIELDNAME = 'field_season_cachecontext';

  public const ALWAYS = '';
  public const SUMMER_SEASON = 'summer';
  public const WINTER_SEASON = 'winter';
  public const NEVER = 'never';

  public static function getSeasons() {
    return [
      self::WINTER_SEASON => t('winter'),
      self::SUMMER_SEASON => t('summer'),
      self::NEVER => t('always hidden'),
    ];
  }

  /**
   * @var \Drupal\Core\Config\ConfigFactory;
   */
  protected $configFactory;

  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    \Drupal::messenger()->addMessage('Hide or show based based on the current season');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $season = self::ALWAYS;
    $config = $this->configFactory->get('system.site');
    $startString = $config->get('season.summer_start');
    $endString = $config->get('season.summer_end');
    if ($startString && $endString) {
      $now = new \Datetime();
      $startDate = \DateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $startString);
      $endDate  = \DateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $endString);
      if ($startDate->format('md') <= $now->format('md') && $endDate->format('md') >= $now->format('md')) {
        $season = self::SUMMER_SEASON;
      }
      else {
        $season = self::WINTER_SEASON;
      }
    }
    return $season;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheTags($this->configFactory->get('system.site.season')->getCacheTags());
    return $cacheable_metadata;
  }
}
