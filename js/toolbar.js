(function ($, Drupal, drupalSettings, once) {

  Drupal.wt = Drupal.wt || {};

  //override core toolbar toogle design and settings
  Drupal.behaviors.wtToolbar = Drupal.behaviors.wtToolbar || {
    attach: function (context, settings) {
      let $elements = $(once('Drupal.behaviors.wtToolbar', '#toolbar-administration', context));
      $elements.each( function() {
        // first time manual init toolbar for users without access to core admin menu (toolbar bug?)
        // due aysnc ajax load do with setTimeout
        window.setTimeout( function() {
          $('.toolbar-horizontal a[id^="toolbar-item-toolbar-menu"]:not(.is-active)').first().click();
          $('button.toolbar-icon-toggle-vertical').first().click();
        }, 1000);
        //expand on toolbar in admin and collapse in frontend
        if (Drupal?.toolbar?.views?.toolbarVisualView) {
          if (!localStorage.getItem('Drupal.toolbar.subtreesHash.wt_admin') && !localStorage.getItem('Drupal.toolbar.subtreesHash.wt_theme') && !localStorage.getItem('Drupal.toolbar.subtreesHash.dgm_admin')) {
            localStorage.setItem('Drupal.toolbar.trayVerticalLocked', 'true');
            Drupal.toolbar.views.toolbarVisualView.model.set({locked: true, orientation: 'vertical'}, {validate: true, override: true});
            let $toolbar = $('#toolbar-item-toolbar-menu-cms').length > 0 ? '"toolbar-item-toolbar-menu-cms"' : '"toolbar-item-toolbar-menu-dgm"';
            localStorage.setItem('Drupal.toolbar.activeTabID', $toolbar);
            Drupal.toolbar.views.toolbarVisualView.model.set({activeTab: document.getElementById(JSON.parse(localStorage.getItem('Drupal.toolbar.activeTabID')))});
          }
          let isAdminArea = drupalSettings.path.currentPathIsAdmin || drupalSettings.wt.isAdminTheme;
          if ( isAdminArea &&
            Drupal?.toolbar?.views?.toolbarVisualView?.model?.attributes?.isFixed &&
            localStorage.getItem('Drupal.toolbar.activeTabID') === null &&
            localStorage.getItem('Drupal.toolbar.lastActiveToolbar')
          ) {
            localStorage.setItem('Drupal.toolbar.activeTabID', localStorage.getItem('Drupal.toolbar.lastActiveToolbar'));
            Drupal.toolbar.views.toolbarVisualView.model.set({activeTab: document.getElementById(JSON.parse(localStorage.getItem('Drupal.toolbar.activeTabID')))});
            localStorage.removeItem('Drupal.toolbar.lastActiveToolbar');
          }
          else if (!isAdminArea) {
            if (localStorage.getItem('Drupal.toolbar.activeTabID')) {
              localStorage.setItem('Drupal.toolbar.lastActiveToolbar', localStorage.getItem('Drupal.toolbar.activeTabID'));
            }
            Drupal.toolbar.views.toolbarVisualView.model.set({activeTab: null});
          }
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings, once);
