<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function wt_cms_token_info() {
  $info['types']['view'] = [
    'name' => t('View', [], ['context' => 'View entity type']),
    'description' => t('Tokens related to views.'),
    'needs-data' => 'view',
  ];
  $info['tokens']['view']['canonical_url'] = [
    'name' => t('Canonical URL'),
    'description' => t('The canonical base URL of the view, without contextual arguments.'),
  ];
  $info['tokens']['view']['first_contextual_filter_url'] = [
    'name' => t('First contextual filter URL'),
    'description' => t('The URL of the view including only the first contextual filter.'),
  ];
  $info['tokens']['node']['last_region'] = [
    'name' => t('Last region'),
    'description' => t('The last selected item of field_region.'),
  ];
  $info['tokens']['node']['last_category_label'] = [
    'name' => t('Last category label'),
    'description' => t('The full name of last selected item of the category ER field.'),
  ];
  $info['tokens']['node']['last_category_slug'] = [
    'name' => t('Last category slug'),
    'description' => t('The transliterated label of last selected item of the category ER field.'),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function wt_cms_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $url_options = ['absolute' => TRUE];
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
  }
  $replacements = [];

  if ($type == 'view' && !empty($data['view'])) {
    /** @var \Drupal\views\ViewExecutable $view */
    $view = $data['view'];

    $bubbleable_metadata->addCacheableDependency($view->storage);

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'canonical_url':
          try {
            if ($url = $view->getUrl([])) {
              $replacements[$original] = $url->setOptions($url_options)
                ->toString();
            }
          }
          catch (\InvalidArgumentException $e) {
            // The view has no URL so we leave the value empty.
            $replacements[$original] = '';
          }
          break;
        case 'first_contextual_filter_url':
          try {
            if ($url = $view->getUrl([])) {
              $result = $url->setOptions($url_options)
                ->toString();
              if (($view->args[0] ?? 'all') != 'all') {
                $result .= '/' . $view->args[0];
              }
              $replacements[$original] = $result;
            }
          }
          catch (\InvalidArgumentException $e) {
            // The view has no URL so we leave the value empty.
            $replacements[$original] = '';
          }
          break;
      }
    }
  }

  if ($type == 'node' && !empty($data['node'])) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $data['node'];

    $bubbleable_metadata->addCacheableDependency($node);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'last_region':
          $replacements[$original] = _wt_cms_tokens_get_entityreference_label($node, 'field_region', -1, TRUE, '-', 0, 0);
          break;
        case 'last_category_slug':
          $fieldname = 'field_' . $node->bundle();
          $replacements[$original] = _wt_cms_tokens_get_entityreference_label($node, $fieldname, -1, TRUE, '-', 30, 50);
          break;
        case 'last_category_label':
          $fieldname = 'field_' . $node->bundle();
          $replacements[$original] = _wt_cms_tokens_get_entityreference_label($node, $fieldname, -1, FALSE, ' ', 100, 0);
          break;
      }
    }
  }

  return $replacements;
}


/**
 * Returns the transliterated label of an entity reference field
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity ... host entity, e.g. node or taxonomy term
 * @param string $fieldname ... the machine name of the entity reference field
 * @param int $offset ... the delta of that field, starting at 0. negative values are counted from the end
 * @param bool $transliterate ... transliterate into lowercase characters without spaces, e.g. "Hällô WORLD" becomes "hallo-world"
 * @param string $placeholder ... the symbol for unknown and non-transliterable characters
 * @param int $softMaxLength ... cut the label at the first placeholder after this position. the label might still be longer than this number. Requires non-empty $placeholder
 * @param int $hardMaxLength ... cut the label at the first at this position. the label will never be longer than this number
 *
 * @return string
 */
function _wt_cms_tokens_get_entityreference_label(EntityInterface $entity, string $fieldname, int $offset = 0, bool $transliterate = TRUE, string $placeholder = '-', int $softMaxLength = 0, int $hardMaxLength = 0) {
  if (!$entity instanceof FieldableEntityInterface) {
    return '';
  }
  if (!$entity->hasField($fieldname)) {
    return '';
  }
  $field = $entity->get($fieldname);
  if (!$field instanceof EntityReferenceFieldItemListInterface) {
    return '';
  }
  if ($field->isEmpty()) {
    return '';
  }
  $refs = $field->referencedEntities();
  if ($offset < 0) {
    $offset = count($refs)  + $offset;
  }
  if (!is_array($refs) || !array_key_exists($offset, $refs)) {
    return '';
  }
  $ref = $refs[$offset];
  if (!$ref instanceof EntityInterface) {
    return '';
  }
  $label = $ref->label();
  if (!$label) {
    return '';
  }
  if ($transliterate) {
    /**
     * @var $transliterationService \Drupal\Core\Transliteration\PhpTransliteration
     */
    $transliterationService = \Drupal::service('transliteration');
    $label = $transliterationService->transliterate($label, LanguageInterface::LANGCODE_DEFAULT, $placeholder);
    $label = strtolower($label);
    $label = preg_replace('/[^a-z0-9_\-]+/', $placeholder, $label);
    $label = preg_replace('/' . preg_quote($placeholder, '/') . '+/', $placeholder, $label);
  }
  if (strlen($label) > $softMaxLength && $placeholder != '') {
    $cutAtPlaceholderAfter = strpos($label, $placeholder, $softMaxLength);
    if ($cutAtPlaceholderAfter) {
      $label = substr($label, 0, $cutAtPlaceholderAfter);
    }
  }
  if (strlen($label) > $hardMaxLength) {
    $label = substr($label, 0, $hardMaxLength);
  }
  return $label;
}
