<?php

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;


/**
 * Implements hook_form_FORM_ID_alter().
 */
function wt_cms_form_user_login_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = '_wt_cms_user_login_redirect';
}

/**
 * Form submission handler for user_login_form().
 *
 * Redirects the user to admin area after logging in.
 */
function _wt_cms_user_login_redirect(&$form, FormStateInterface $form_state) {
  $roles = \Drupal::currentUser()->getRoles();

  // Check if a destination was set, probably on an exception controller.
  // @see \Drupal\user\Form\UserLoginForm::submitForm()
  $request = \Drupal::service('request_stack')->getCurrentRequest();
  if (!$request->request->has('destination')) {
    if (in_array('editor', $roles)){
      $form_state->setRedirect('wt_cms.admin.cms');
    }
    else {
      $form_state->setRedirect('<front>');
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function wt_cms_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /**
   * remove the bloated message from
   * @see Drupal\layout_builder\Form\OverridesEntityForm::buildMessage()
   * and add it as a button to form actions instead
   */
  if (substr($form_id, -1 * strlen('_layout_builder_form')) === '_layout_builder_form') {
    $layoutLink = FALSE;
    if ($form['layout_builder_message']['message']['#message_list']['status'][0] ?? null) {
      /** @var $msg TranslatableMarkup */
      $msg = $form['layout_builder_message']['message']['#message_list']['status'][0];
      if ($msg instanceof TranslatableMarkup) {
        if ($msg->getUntranslatedString() == 'You are editing the layout template for all @bundle @plural_label.') {
          unset($form['layout_builder_message']);
        }
        elseif ($msg->getUntranslatedString() == 'You are editing the layout for this @bundle @singular_label. <a href=":link">Edit the template for all @bundle @plural_label instead.</a>' && ($msg->getArguments()[':link'] ?? NULL)) {
          $layoutLink = $msg->getArguments()[':link'];
          $form['actions']['goto_global_layout'] = [
            '#type' => 'link',
            '#url' => Url::fromUri("internal:{$layoutLink}"),
            '#title' => t('Edit global layout'),
            '#weight' => 16,
            '#attributes' => ['class' => ['button']]
          ];
          unset($form['layout_builder_message']);
        }
      }
    }
    if ($form['actions']['revert'] ?? null) {
      $form['actions']['revert']['#value'] = t('Revert to global layout');
    }
    if ($form['advanced'] ?? null) {
      if (!array_key_exists('#attributes', $form['advanced'])) {
        $form['advanced']['#attributes'] = [];
      }
      if (!array_key_exists('class', $form['advanced']['#attributes'])) {
        $form['advanced']['#attributes']['class'] = [];
      }
      $form['advanced']['#attributes']['class'][] = 'visually-hidden';
    }
  }

  /**
   * always hide (not remove) revision options
   */
  if ($form['revision_information'] ?? null) {
    if (!array_key_exists('#attributes', $form['revision_information'])) {
      $form['revision_information']['#attributes'] = [];
    }
    if (!array_key_exists('class', $form['revision_information']['#attributes'])) {
      $form['revision_information']['#attributes']['class'] = [];
    }
    $form['revision_information']['#attributes']['class'][] = 'visually-hidden';
    //also hide tab link if rev.info is inside tabs
    if ($form['revision_information']['#group'] ?? NULL == 'advanced') {
      unset($form['revision_information']['#group']);
    }
  }
}


/**
 * @see https://git.drupalcode.org/project/inline_block_title_automatic/-/blob/8.x-1.x/src/FormAlter.php
 *
 * Block content entities in layout builder suffer from a minor UI headache with
 * regards to titles. All block content entities require the "info" field to
 * have a value. In the reusable library world, this is labelled "Block
 * description". This is surfaced in the admin UIs when searching through the
 * block library as the administrative label of the entity, thus it is far
 * useful to have a descriptive label such as "winter recycling campaign 2018"
 * than something editorial such as "Recycle now!". These block descriptions
 * are also required for any component that may not have a clear "title" element
 * to display.
 *
 * For inline blocks and reusable blocks, a "placement" label is also always
 * required. For reusable blocks, this placement handily defaults to the "info"
 * field described above, but can be changed! For inline blocks, this is
 * mandatory and is used to populate the info field when a block is created,
 * thus the placement label always equals the "info" label.
 */

function _wt_cms_form_layout_builder_form_hide_label(&$form, FormStateInterface $form_state, $form_id) {
  // Hide the label of the block placement and provide a default value if it
  // is empty.
  $form['settings']['label']['#type'] = 'value';
  if (empty($form['settings']['label']['#default_value'])) {
    $form['settings']['label']['#default_value'] = $form['settings']['admin_label']['#plain_text'];
  }

  // Default to hiding the label of the block.
  $form['settings']['label_display']['#default_value'] = FALSE;
  $form['settings']['label_display']['#type'] = 'value';
}

/**
 * Implements hook_form_FORM_ID_alter() for layout_builder_update_block.
 */
function wt_cms_form_layout_builder_update_block_alter(&$form, FormStateInterface $form_state, $form_id) {
  _wt_cms_form_layout_builder_form_hide_label($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter() for layout_builder_add_block.
 */
function wt_cms_form_layout_builder_add_block_alter(&$form, FormStateInterface $form_state, $form_id) {
  _wt_cms_form_layout_builder_form_hide_label($form, $form_state, $form_id);
}

/**
 * Alter Accordions
 */
function wt_cms_field_widget_single_element_text_textarea_with_summary_form_alter(&$element, FormStateInterface $form_state, $context) {
  $fieldItemList = $context['items'];
  if ($fieldItemList->getName() == 'field_texts' && $fieldItemList->getEntity() instanceof BlockContentInterface && $fieldItemList->getEntity()->bundle() == 'accordion') {
    //unset($element['summary']['#attributes']);
    unset($element['summary']['#description']);
    $element['summary']['#title'] = t('Title');
    $element['summary']['#required'] = TRUE;
    $element['#title'] = t('Content');
    $element['#title_display'] = 'before';
  }
}


// Alter label of text inputs of image field
// https://stackoverflow.com/questions/18409085/drupal-7-how-to-alter-image-field-widget-alt-or-title-label
function wt_cms_field_widget_single_element_form_alter(&$element, FormStateInterface $form_state, $context) {
  if ($context['widget'] instanceof ImageWidget) {
    $element['#process'][] = '_wt_cms_image_field_widget_process';
  }
}

function _wt_cms_image_field_widget_process($element, FormStateInterface &$form_state, $form) {
  if (isset($element['title'])) {
    $element['title']['#title'] = t('Copyright');
    $element['title']['#description'] = t('');
  }
  return $element;
}

