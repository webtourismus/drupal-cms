<?php

use Drupal\block\BlockInterface;
use Drupal\block\Entity\Block;
use Drupal\block_content\BlockContentInterface;

function wt_cms_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  if (($variables['elements']['content']['#block_content'] ?? NULL) instanceOf BlockContentInterface){
    /** @var $contentBlock BlockContentInterface */
    $contentBlock = $variables['elements']['content']['#block_content'];

    if ($viewMode = $variables['elements']['#configuration']['view_mode']) {
      array_splice($suggestions, 2, 0, 'block__block_content__' . str_replace('-', '_', $contentBlock->bundle()) . '__' . $viewMode);
    }
    array_splice($suggestions, 2, 0, 'block__block_content__' . str_replace('-', '_', $contentBlock->bundle()));
    $uniqueIdSuggestion = 'block__block_content__' . str_replace('-', '_', $contentBlock->uuid());
    if (!in_array($uniqueIdSuggestion, $suggestions)) {
      array_push($suggestions, $uniqueIdSuggestion);
    }
    if ($contentBlock->id()) {
      array_splice($suggestions, -1, 0, 'block__block_content__' . $contentBlock->id());
    }
  }

  if ($variables['elements']['#id'] ?? NULL) {
    $block = Block::load($variables['elements']['#id']);
    if ($block instanceof BlockInterface) {
      $region = $block->getRegion();
      array_unshift($suggestions, 'block__region__' . str_replace('-', '_', $region));
    }
  }

  if (($variables['elements']['#base_plugin_id'] ?? NULL) == 'field_block') {
    $derivPluginId = $variables['elements']['#derivative_plugin_id'] ?? NULL;
    if ($derivPluginId) {
      $tmpArr = explode(':', $derivPluginId);
      if (count($tmpArr) == 3) {
        [$entity, $bundle, $field] = $tmpArr;
        array_splice($suggestions, -2, 0, 'block__field_block__' . str_replace('-', '_', $field));

        if ($entity) {
          array_splice($suggestions, -1, 0, "block__field_block__{$entity}__" . str_replace('-', '_', $field));
        }
      }
    }
  }

  if (($variables['elements']['#base_plugin_id'] ?? NULL) == 'entity_view') {
    $entityType = $variables['elements']['#derivative_plugin_id'];
    $viewMode = $variables['elements']['content']['#view_mode'];
    $bundle = NULL;
    if (array_key_exists('#node', $variables['elements']['content'])) {
      $bundle = $variables['elements']['content']['#node']->bundle();
    }
    $suggestions[] = 'block__entity_view__' . $viewMode;
    $suggestions[] = "block__entity_view__{$entityType}__{$viewMode}";
    if ($bundle) {
      $suggestions[] = "block__entity_view__{$entityType}__{$bundle}";
      $suggestions[] = "block__entity_view__{$entityType}__{$bundle}__{$viewMode}";
    }
  }
}

function wt_cms_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  /**
   * @TODO field template suggestions based on "layout builder styles" module
   */
  $element = $variables['element'];
  $entity = $element['#object'];
  $viewMode = $element['#third_party_settings']['layout_builder']['view_mode'] ?? $element['#view_mode'];
  $suggestions[] = 'field__' . $element['#entity_type'] . '__' . str_replace('-', '_', $element['#field_name']) . '__' . $viewMode;
  $suggestions[] = 'field__' . $element['#entity_type'] . '__' . str_replace('-', '_', $element['#field_name']) . '__' . $element['#bundle'] . '__' . $viewMode;
  if ($entity->hasField('field_template')) {
    if ($templateType = $entity->get('field_template')->value) {
      array_splice($suggestions, 3, 0, 'field__' . $templateType . '__' . $viewMode);
      array_splice($suggestions, 3, 0, 'field__' . $templateType);
      $suggestions[] = 'field__' . $element['#entity_type'] . '__' . $templateType . '__' . str_replace('-', '_', $element['#field_name']);
      $suggestions[] = 'field__' . $element['#entity_type'] . '__' . $templateType . '__' . str_replace('-', '_', $element['#field_name']) . '__' . $viewMode;
    }
  }
  array_splice($suggestions, 2, 0, 'field__' . str_replace('-', '_', $element['#field_name']) . '__' . $viewMode);
}
