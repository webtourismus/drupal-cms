<?php


use Drupal\Core\Form\FormStateInterface;

function wt_cms_form_system_site_information_settings_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $site_config = \Drupal::config('system.site');

  $form['common_links'] = [
    '#type' => 'details',
    '#title' => t('Commonly linked pages'),
    '#open' => TRUE,
  ];
  $form['common_links']['page_enquire'] = [
    '#type' => 'textfield',
    '#title' => t('Page with enquiry form'),
    '#default_value' => $site_config->get('page.enquire'),
    '#size' => 40,
    '#description' => t('This page is used by templates that render a contextual enquiry form link, like rooms or holiday packages.'),
  ];
  $form['common_links']['page_book'] = [
    '#type' => 'textfield',
    '#title' => t('Page with booking form'),
    '#default_value' => $site_config->get('page.book'),
    '#size' => 40,
    '#description' => t('This page is used by templates that render a contextual booking link, like rooms or holiday packages.'),
  ];

  $form['#validate'][] = '_wt_cms_validate_system_site_common_links_settings';
  $form['#submit'][] = '_wt_cms_save_system_site_common_links_settings';

  $form['season'] = [
    '#type' => 'details',
    '#title' => t('Seasons'),
    '#open' => TRUE,
  ];
  $form['season']['info'] = [
    '#type' => 'item',
    '#markup' => t('Year doesn\'t matter, only day and month are evaluated.'),
  ];
  $form['season']['summer_start'] = [
    '#type' => 'date',
    '#title' => t('Summer start'),
    '#default_value' => $site_config->get('season.summer_start', '2000-04-31'),
    '#required' => TRUE,
  ];
  $form['season']['summer_end'] = [
    '#type' => 'date',
    '#title' => t('Summer end'),
    '#default_value' => $site_config->get('season.summer_end', '2000-09-30'),
    '#required' => TRUE,
  ];

  $form['#validate'][] = '_wt_cms_validate_system_site_season_settings';
  $form['#submit'][] = '_wt_cms_save_system_site_season_settings';
}

function _wt_cms_save_system_site_common_links_settings($form, FormStateInterface $form_state) {
  \Drupal::configFactory()->getEditable('system.site')
    ->set('page.enquire', $form_state->getValue('page_enquire'))
    ->set('page.book', $form_state->getValue('page_book'))
    ->save();
}

function _wt_cms_validate_system_site_common_links_settings(&$form, FormStateInterface $form_state) {
  /**
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  $aliasManager = \Drupal::service('path_alias.manager');
  /**
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  $pathValidator = \Drupal::service('path.validator');

  if (!$form_state->isValueEmpty('page_enquire')) {
    $form_state->setValueForElement($form['common_links']['page_enquire'], $aliasManager->getPathByAlias($form_state->getValue('page_enquire')));
  }
  if (($value = $form_state->getValue('page_enquire')) && $value[0] !== '/') {
    $form_state->setErrorByName('page_enquire', t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('page_enquire')]));
  }
  if (!$form_state->isValueEmpty('page_enquire') && !$pathValidator->isValid($form_state->getValue('page_enquire'))) {
    $form_state->setErrorByName('page_enquire', t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $form_state->getValue('page_enquire')]));
  }

  if (!$form_state->isValueEmpty('page_book')) {
    $form_state->setValueForElement($form['common_links']['page_book'], $aliasManager->getPathByAlias($form_state->getValue('page_book')));
  }
  if (($value = $form_state->getValue('page_book')) && $value[0] !== '/') {
    $form_state->setErrorByName('page_book', t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('page_book')]));
  }
  if (!$form_state->isValueEmpty('page_book') && !$pathValidator->isValid($form_state->getValue('page_book'))) {
    $form_state->setErrorByName('page_book', t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $form_state->getValue('page_book')]));
  }
}

function _wt_cms_save_system_site_season_settings($form, FormStateInterface $form_state) {
  \Drupal::configFactory()->getEditable('system.site')
    ->set('season.summer_start', $form_state->getValue('summer_start'))
    ->set('season.summer_end', $form_state->getValue('summer_end'))
    ->save();
}

function _wt_cms_validate_system_site_season_settings(&$form, FormStateInterface $form_state) {
  $startDate = \DateTime::createFromFormat(\Drupal\datetime\Plugin\Field\FieldType\DateTimeItem::DATE_STORAGE_FORMAT, $form_state->getValue('summer_start'))->format('md');
  $endDate = \DateTime::createFromFormat(\Drupal\datetime\Plugin\Field\FieldType\DateTimeItem::DATE_STORAGE_FORMAT, $form_state->getValue('summer_end'))->format('md');
  if ($endDate < $startDate) {
    $form_state->setErrorByName('season', t('Summer end must be later in year than summer start.'));
  }
}
