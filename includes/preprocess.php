<?php

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;

function wt_cms_template_preprocess_default_variables_alter(&$variables) {
  // we want the language variable in every template
  $variables['wt']['language'] = \Drupal::languageManager()->getCurrentLanguage();
}

function wt_cms_preprocess_html(array &$variables) {
  if ($variables['is_front'] ?? NULL) {
    $variables['attributes']['class'][] = 'body--front';
  }

  if ($variables['logged_in'] ?? NULL) {
    $variables['attributes']['class'][] = 'body--authenticated';
  }

  $routeMatch = \Drupal::routeMatch();
  /** @var $cmsHelper Drupal\wt_cms\CmsHelper */
  $cmsHelper = \Drupal::service('wt_cms.helper');

  if ($node = $cmsHelper->getNodeFromRoute()) {
    $variables['node'] = $node;
    $variables['attributes']['class'][] = 'body--node';
    $variables['attributes']['class'][] = 'body--node-' . $node->id();
    $variables['attributes']['class'][] = 'body--node-' . $node->bundle();
  }

  if (in_array($routeMatch->getRouteName(), ['user.login', 'user.pass', 'user.reset.form', 'user.register'])) {
    $variables['attributes']['id'] = 'body--login';
    $variables['attributes']['class'][] = 'body--login';
    $variables['attributes']['class'][] = 'body--login-' . explode($routeMatch->getRouteName(), '.')[1];
  }

  if ($viewInfos = $cmsHelper->getViewIdsFromRoute()) {
    $variables['wt']['views'] = $viewInfos;
    $variables['attributes']['class'][] = 'body--views';
    $variables['attributes']['class'][] = 'body--views--' . $viewInfos['views_id'];
    $variables['attributes']['class'][] = 'body--views--' . $viewInfos['views_id'] . '--' . $viewInfos['display_id'];
  }

  $variables['#attached']['library'][] = 'core/drupalSettings';
  $isAdminTheme = (\Drupal::service('theme.manager')->getActiveTheme()->getName() == \Drupal::config('system.theme')->get('admin'));
  $variables['#attached']['drupalSettings']['wt']['isAdminTheme'] = $isAdminTheme;
}

function wt_cms_preprocess_page(array &$variables) {
  /** @var  $helper Drupal\wt_cms\CmsHelper */
  $cmsHelper = \Drupal::service('wt_cms.helper');
  if ($node = $cmsHelper->getNodeFromRoute()) {
    $variables['node'] = $node;
  }
  if ($viewInfos = $cmsHelper->getViewIdsFromRoute()) {
    $variables['wt']['views'] = $viewInfos;
  }
}

function wt_cms_preprocess_region(array &$variables) {
  $variables['attributes']['class'] = array_merge($variables['attributes']['class'] ?? [], [
    'region',
    'region--' . $variables['region'],
  ]);
}

function wt_cms_preprocess_media(array &$variables) {
  $variables['attributes']['class'] = array_merge($variables['attributes']['class'] ?? [], [
    'media',
    'media--' . $variables['view_mode'],
    'media--' . $variables['media']->bundle(),
    'media--' . $variables['media']->bundle() . '--' . $variables['view_mode'],
    'media--' . $variables['media']->id(),
  ]);
}

function wt_cms_preprocess_node(array &$variables) {
  $variables['attributes']['class'] = array_merge($variables['attributes']['class'] ?? [], [
    'node',
    'node--' . $variables['view_mode'],
    'node--' . $variables['node']->bundle(),
    'node--' . $variables['node']->bundle() . '--' . $variables['view_mode'],
    'node--' . $variables['node']->id(),
    $variables['node']->bundle(),
    $variables['node']->bundle()  . '--' . $variables['view_mode'],
  ]);
}

function wt_cms_preprocess_block(array &$variables) {
  _wt_cms_preprocess_block_defaultClasses($variables);
  _wt_cms_preprocess_block_cToolsEntityViewBlockClasses($variables);
  _wt_cms_preprocess_block_blockContentClasses($variables);
  _wt_cms_preprocess_block_fieldBlock_layoutBuilderStylesAttributes($variables);

  _wt_cms_preprocess_block_mediaImageViewmode($variables);
  _wt_cms_preprocess_block_linkblock($variables);

  _wt_cms_preprocess_block_removeContextualLinksInFrontend($variables);
}

function _wt_cms_preprocess_block_defaultClasses(array &$variables) {
  $variables['attributes']['class'] = array_merge($variables['attributes']['class'] ?? [], [
    'block',
    'block--' . $variables['base_plugin_id'],
    ($variables['derivative_plugin_id'] ? 'block--' . $variables['base_plugin_id'] . '--' . str_replace(':', '-', $variables['derivative_plugin_id']) : NULL),
    ($variables['attributes']['id'] ?? NULL) ? 'block--id-' . str_replace('block-', '', $variables['attributes']['id']) : NULL,
  ]);
}

function _wt_cms_preprocess_block_cToolsEntityViewBlockClasses(array &$variables) {
  if (($variables['elements']['#base_plugin_id'] ?? NULL) != 'entity_view') {
    return;
  }

  $entityType = $variables['elements']['#derivative_plugin_id'];
  $viewMode = $variables['elements']['content']['#view_mode'];
  $bundle = NULL;
  if (array_key_exists('#node', $variables['elements']['content'])) {
    $bundle = $variables['elements']['content']['#node']->bundle();
  }
  $variables['attributes']['class'][] = "block--entity_view--{$viewMode}";
  $variables['attributes']['class'][] = "block--entity_view--{$entityType}--{$viewMode}";
  if ($bundle) {
    $variables['attributes']['class'][] = "block--entity_view--{$entityType}--{$bundle}";
    $variables['attributes']['class'][] = "block--entity_view--{$entityType}--{$bundle}--{$viewMode}";
  }
}

function _wt_cms_preprocess_block_blockContentClasses(array &$variables) {
  if (!(($variables['elements']['content']['#block_content'] ?? NULL) instanceof BlockContentInterface)) {
    return;
  }

  /** @var $block_content BlockContentInterface */
  $block_content = $variables['elements']['content']['#block_content'];
  //make "block_content" $variables similar to other content entities like "node"
  $variables['bc'] = $block_content;
  $variables['view_mode'] = $variables['content']['#view_mode'];

  // When displaying block titles from custom content blocks, we want the
  // title from custom content block library and not the title from block
  // placement configuration, to get proper translated block titles
  if ($variables['label']) {
    $variables['label'] = $block_content->label();
  }

  if ($variables['attributes']['id'] ?? NULL) {
    $variables['attributes']['class'][] = str_replace('block-', '', $variables['attributes']['id']);
  }

  $variables['attributes']['class'] = array_merge($variables['attributes']['class'], [
    'bc-' . $block_content->bundle(),
    'bc-' . $block_content->bundle() . '--' . $variables['view_mode'],
    'bc--' . $block_content->id(),
  ]);
}

function _wt_cms_preprocess_block_fieldBlock_layoutBuilderStylesAttributes(array &$variables) {
  if (($variables['elements']['#base_plugin_id'] ?? NULL) != 'field_block') {
    return;
  }

  if ($id = ($variables['elements']['#layout_builder_style'][0] ?? NULL)) {
    $variables['attributes']['data-layout_builder_style-id'] = $id;
  }
}

/**
 * Image ratio for image fields, based on media entity view mode
 */
function _wt_cms_preprocess_block_mediaImageViewmode(array &$variables, $imageFieldname = 'field_images', $viewmodeFieldname = 'field_image_viewmode') {
  $hostEntity = $variables['elements']['content']['#block_content'] ?? NULL;

  if (!($hostEntity instanceof BlockContentInterface)) {
    return;
  }
  if (!$hostEntity->hasField($imageFieldname) || !$hostEntity->hasField($viewmodeFieldname)) {
    return;
  }
  if (!$hostEntity->get($imageFieldname)->getFieldDefinition() == 'entity_reference') {
    return;
  }
  $mediaEntities = $hostEntity->get($imageFieldname)->referencedEntities();
  $mediaEntity = reset($mediaEntities);
  if (!$mediaEntity) {
    return;
  }

  $newViewMode = $hostEntity->get($viewmodeFieldname)->value;
  $availableViewModes =  \Drupal::service('entity_display.repository')->getViewModeOptionsByBundle($mediaEntity->getEntityTypeId(), $mediaEntity->bundle());
  if (!in_array($newViewMode, array_keys($availableViewModes))) {
    \Drupal::logger('wt_cms')->warning("Invalid view mode \"$newViewMode\": host field {$hostEntity->getEntityTypeId()} {$hostEntity->id()}) / $imageFieldname, referenced entity {$mediaEntity->getEntityTypeId()} {$mediaEntity->id()}");
    return;
  }

  foreach (Element::children($variables['content']['field_images']) as $key) {
    //set the view mode selected by the editor
    $variables['content'][$imageFieldname][$key]['#view_mode'] = $newViewMode;
    //referenced media entities are cached on their own,
    //but this cache got invalid due viewmode change above
    unset($variables['content'][$imageFieldname][$key]['#cache']['keys']);
  }
}

function _wt_cms_preprocess_block_linkblock(array &$variables) {
  $hostEntity = $variables['elements']['content']['#block_content'] ?? NULL;

  if (!($hostEntity instanceof BlockContentInterface)) {
    return;
  }
  if ($hostEntity->bundle() != 'link') {
    return;
  }
  if ($hostEntity->get('field_link')->isEmpty()) {
    return;
  }

  $link = $hostEntity->get('field_link')->first()->getUrl();
  if (!$link->isRouted()) {
    return;
  }

  $route = $link->getRouteParameters();
  $entityType = key($route);
  $referencedEntity = NULL;
  try {
    $referencedEntity = \Drupal::entityTypeManager()
      ->getStorage($entityType)
      ->load($route[$entityType]);
    $referencedEntity = \Drupal::service('entity.repository')
      ->getTranslationFromContext($referencedEntity);
  }
  catch (Throwable $t) {
    $referencedEntity = NULL;
  }

  if (!$referencedEntity) {
    return;
  }

  $variables['target_entity'] = $referencedEntity;

  // when linking to file media entities, we want direct links/downloads,
  // not links to the full view of the media entity
  if ($referencedEntity->getEntityTypeId() == 'media' && $referencedEntity->bundle() == 'file' && $referencedEntity->hasField('field_media_file')) {
    $variables['content']['field_link'][0]['#url'] = \Drupal\Core\Url::fromUri($referencedEntity->get('field_media_file')->entity->url());
  }

  // fetch remote title if block title field is empty
  if ($hostEntity->get('field_title')->isEmpty() && ($referencedEntity->hasField('title') || $referencedEntity->hasField('name'))) {
    // media entities use "name", nodes use "title"
    if ($field = $referencedEntity->title ?: $referencedEntity->name) {
      // paragraph--link.html needs the pure title value without full field markup
      $variables['content']['field_title'][0]['#type'] = 'markup';
      //$variables['content']['field_title']['#plain_text'] = $referencedEntity->title->value;
      $variables['content']['field_title'][0]['#markup'] = $field->value;
      $variables['content']['field_title'][0]['#cache']['tags'] = $referencedEntity->getCacheTags();
    }
  }

  // fetch remote short description if block text field is empty
  if ($hostEntity->get('field_text')->isEmpty() && $referencedEntity->hasField('body')) {
    $field = $referencedEntity->get('body');
    if (!$field->isEmpty()) {
      $display = \Drupal::entityTypeManager()
        ->getStorage('entity_view_display')
        ->load('block_content.link.' . $variables['view_mode']);
      $displayComponents = $display->getComponent('field_text');
      $fieldRenderable = \Drupal::entityTypeManager()
        ->getViewBuilder('block_content')
        ->viewField($field, $displayComponents);
      $fieldMarkup = \Drupal::service('renderer')->render($fieldRenderable, FALSE);
      $variables['content']['field_text'][0]['#type'] = 'markup';
      $variables['content']['field_text'][0]['#markup'] = $fieldMarkup;
      $variables['content']['field_text'][0]['#cache']['tags'] = $referencedEntity->getCacheTags();
    }
  }

  // fetch remote images if block image field is empty
  if ($hostEntity->get('field_images')->isEmpty()) {
    $imageViewMode = $hostEntity->field_image_viewmode->value;
    $display = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load('media.image.' . $imageViewMode);
    $displayComponents = $display->getComponent('field_media_image');
    $displayComponents['settings']['view_mode'] = $imageViewMode;

    if ($referencedEntity->hasField('field_images') && !$referencedEntity->get('field_images')->isEmpty()) {
      $mediaEntities = $referencedEntity->get('field_images')->referencedEntities();
      $mediaEntityForRendering = $mediaEntities[0];
      foreach ($mediaEntities as $idx => $mediaEntity) {
        $mediaEntityForRendering->get('field_media_image')->set($idx, $mediaEntity->get('field_media_image')->first());
      }
      // our modified responsive image formatter needs a referring item to calculate the img[sizes] attribute
      // (which depends on the referring field's sibling field_contentwidth_XXX, which needs to be taken from original, hosting paragraph)
      $fieldRenderable = \Drupal::entityTypeManager()
        ->getViewBuilder('media')
        ->viewField($mediaEntityForRendering->get('field_media_image'), $displayComponents);
      $fieldMarkup = \Drupal::service('renderer')->render($fieldRenderable, FALSE);
      $variables['content']['field_images'] = [
        '0' => [
          '#type' => 'markup',
          '#markup' => $fieldMarkup,
          '#cache' => [['tags' => $referencedEntity->getCacheTags()]],
        ]
      ];
    }
    elseif ($referencedEntity->hasField('field_imagefiles') && !$referencedEntity->get('field_imagefiles')->isEmpty()) {
      $imageField = $referencedEntity->get('field_imagefiles');
      $fieldRenderable = \Drupal::entityTypeManager()
        ->getViewBuilder('media')
        ->viewField($imageField, $displayComponents);
      $fieldMarkup = \Drupal::service('renderer')->render($fieldRenderable, FALSE);
      $variables['content']['field_images'] = [
        '0' => [
          '#type' => 'markup',
          '#markup' => $fieldMarkup,
          '#cache' => [['tags' => $referencedEntity->getCacheTags()]],
        ]
      ];
    }
  }
}

function _wt_cms_preprocess_block_removeContextualLinksInFrontend(&$variables) {
  /**
   * Suppress all contextual quickedit stuff on frontend themes, we only want it
   * for layout builder in admin
   */
  if ($variables['title_suffix']['contextual_links'] ?? FALSE) {
    $routeName = \Drupal::routeMatch()->getRouteName();
    $isAdminTheme = (\Drupal::service('theme.manager')->getActiveTheme()->getName() == \Drupal::config('system.theme')->get('admin'));
    if (!in_array($routeName, WT_CMS_LAYOUT_BUILDER_ROUTES) && !$isAdminTheme) {
      unset($variables['title_suffix']['contextual_links']);
    }
  }

}


function wt_cms_preprocess_field(array &$variables, $hook) {
  _wt_cms_preprocess_field__addDefaultClasses($variables, $hook);
  _wt_cms_preprocess_field__addNodeFieldClasses($variables, $hook);
  _wt_cms_preprocess_field__addBlockcontentFieldClasses($variables, $hook);
  _wt_cms_preprocess_field__addFormattedTextClasses($variables, $hook);
}

function _wt_cms_preprocess_field__addDefaultClasses(array &$variables, $hook) {
  $parent = $variables['element'];

  $variables['attributes']['class'][] = 'field';
  $variables['attributes']['class'][] = 'field--' . str_replace('field_', '', $variables['field_name']);
  $variables['attributes']['class'][] = 'field--type-' . $variables['field_type'];

  if (!array_key_exists('title_attributes', $variables)) {
    $variables['title_attributes'] = [];
  }
  if (!array_key_exists('class', $variables['title_attributes'])) {
    $variables['title_attributes']['class'] = [];
  }
  $variables['title_attributes']['class'][] = 'field__label';
  $variables['title_attributes']['class'][] = 'field__label--' . str_replace('field_', '', $variables['field_name']);
  $variables['title_attributes']['class'][] = 'field__label--type-' . $variables['field_type'];

  $variables['attributes']['class'][] = $variables['entity_type'] . '__' . str_replace('field_', '', $variables['field_name']);
  $variables['attributes']['class'][] = $variables['entity_type'] . '__' . str_replace('field_', '', $variables['field_name']) . '--' . $parent['#bundle'];

  foreach ($variables['items'] as $idx => $item) {
    if (!isset($variables['items'][$idx]['attributes'])) {
      $variables['items'][$idx]['attributes'] = ['class' => []];
    }
    if (!isset( $variables['items'][$idx]['attributes']['class'])) {
      $variables['items'][$idx]['attributes']['class'] = [];
    }
    $variables['items'][$idx]['attributes']['class'][] = 'field__item';
    $variables['items'][$idx]['attributes']['class'][] = 'field__item--' . str_replace('field_', '', $variables['field_name']);
    $variables['items'][$idx]['attributes']['class'][] = 'field__item--type-' . $variables['field_type'];
    if (count($variables['items']) == 1) {
      $variables['items'][$idx]['attributes']['class'][] = 'field__item--single';
    }
    else {
      $variables['items'][$idx]['attributes']['class'][] = 'field__item--multiple';
    }
    $variables['items'][$idx]['attributes']['class'][] = "field__item--{$idx}";
    if ($idx == count($variables['items']) - 1) {
      $variables['items'][$idx]['attributes']['class'][] = 'field__item--last';
    }
  }
}

function _wt_cms_preprocess_field__addNodeFieldClasses(array &$variables, $hook) {
  $parent = $variables['element'];
  /**
   * @var $parentEntity \Drupal\Core\Entity\ContentEntityBase;
   */
  $parentEntity = $parent['#object'];
  if ($parentEntity->getEntityTypeId() != 'node') {
    return;
  }

  /**
   * nodes are our main content entities, so we use BEM-style using the bundle as blockname
   * and the field names as elements
   */
  $variables['attributes']['class'][] = $parent['#bundle'] . '__' . str_replace('field_', '', $variables['field_name']);
  $viewmode = $parent['#view_mode'];
  //layout builder uses a custom view mode for inline content field
  if ($parent['#third_party_settings']['layout_builder']['view_mode'] ?? NULL) {
    $viewmode = $parent['#third_party_settings']['layout_builder']['view_mode'];
  }
  //normalize viewmode 'default' to 'full', as it is used on top entity (node) level
  if ($viewmode == 'default') {
    $viewmode = 'full';
  }
  $variables['attributes']['class'][] = $parent['#bundle'] . '__' . str_replace('field_', '', $variables['field_name']) . '--' . $viewmode;
}

function _wt_cms_preprocess_field__addBlockcontentFieldClasses(array &$variables, $hook) {
  $parent = $variables['element'];
  /**
   * @var $parentEntity \Drupal\Core\Entity\ContentEntityBase;
   */
  $parentEntity = $parent['#object'];

  if ($parentEntity->getEntityTypeId() != 'block_content') {
    return;
  }

  /**
   * custom content blocks use a BEM-style naming similar to nodes, but
   * the block name is prefixed with "bc-"
   */
  $variables['attributes']['class'][] = 'bc-' . $parent['#bundle'] . '__' . str_replace('field_', '', $variables['field_name']);
  $viewmode = $parent['#view_mode'];
  //normalize viewmode 'default' to 'full', as it is used on top entity (node) level
  if ($viewmode == 'default') {
    $viewmode = 'full';
  }
  $variables['attributes']['class'][] = 'bc-' . $parent['#bundle'] . '__' . str_replace('field_', '', $variables['field_name']) . '--' . $viewmode;
}

function _wt_cms_preprocess_field__addFormattedTextClasses(array &$variables, $hook) {
  if (!in_array($variables['field_type'], [
    'text',
    'text_long',
    'text_with_summary',
  ])) {
    return;
  }
  $variables['attributes']['class'][] = 'cktext';
}

function wt_cms_preprocess_paragraph__banner(array &$variables) {
  $paragraph = $variables['paragraph'];
  $parent = $paragraph->getParentEntity();
  if (is_null($parent) && \Drupal::routeMatch()
      ->getRouteName() == 'entity.node.preview') {
    // this is an assumption and a workaround for a specific usecase and
    // might need rework if content/paragraph structure changes
    $parent = \Drupal::routeMatch()->getParameter('node_preview');
  }

  if ($parent && $parent->hasField('field_banner_viewmode')) {
    $imageViewMode = $parent->field_banner_viewmode->value;
    if (isset($variables['content']['field_image']['0']['#view_mode'])) {
      $variables['content']['field_image']['0']['#view_mode'] = $imageViewMode;
      unset($variables['content']['field_image']['0']['#cache']['keys']);
    }
  }
}

/**
 * For our custom admin menu, we use the the special icon_class attribute
 *
 * Implements template_preprocess_menu().
 *
 * @see 'templates/navigation/menu.html.twig' for additional classes on ul
 */
function wt_cms_preprocess_menu(array &$variables) {
  // "unplaced" menus (not in a region, like admin toolbar menu) don't have a name
  if (!isset($variables['menu_name'])) {
    $variables['menu_name'] = str_replace(['menu__', '__'], ['', '-'], $variables['theme_hook_original']);
  }
  $attachColorbox = 0;
  $variables['attributes']['class'] = [
    'nav__ul',
    'nav__ul--' . $variables['menu_name'],
  ];
  _wt_cms_menuAddAttributes($variables['items'], 0, $variables['menu_name'], $attachColorbox);
  if ($attachColorbox) {
    // @TODO replace with ...glightbox... ?
    //$variables['#attached']['library'][] = 'wtfrontend_base/colorbox';
  }
}

// recursive helper function for menu preprocessing
function _wt_cms_menuAddAttributes(&$ul, $level, $modifier, &$attachColorbox) {
  foreach ($ul as $idx => $li) {
    if (array_key_exists('url', $li)) {
      if (!$attr = $li['url']->getOption('attributes')) {
        $ul[$idx]['url']->setOption('attributes', ['class' => []]);
        $attr = $li['url']->getOption('attributes');
      }
      $attr['class'][] = 'nav__a';
      $attr['class'][] = "nav__a--level-{$level}";
      $attr['class'][] = "nav__a--{$modifier}";
      $attr['class'][] = "nav__a--{$modifier}-{$level}";
      $ul[$idx]['url']->setOption('attributes', $attr);

      $rewriteTitle = FALSE;
      if ($attr['data-allow-html'] ?? NULL) {
        $rewriteTitle = $li['title'];
      }
      if ($attr['data-inject-icon'] ?? NULL) {
        $rewriteTitle = '<i class="' . $attr['data-inject-icon'] . '"></i>' . $li['title'];
      }
      if ($rewriteTitle) {
        $ul[$idx]['title'] = Drupal\Core\Render\Markup::create($rewriteTitle);
      }
      if (array_key_exists('target', $attr) && $attr['target'] == 'modal') {
        $attachColorbox++;
      }
    }

    $ul[$idx]['attributes']['class'] = [
      'nav__li',
      "nav__li--level-{$level}",
      "nav__li--{$modifier}",
      "nav__li--{$modifier}-{$level}",
    ];
    if ($li['in_active_trail'] ?? NULL) {
      $ul[$idx]['attributes']['class'][] = 'is-active';
    }
    if (empty($li['below'])) {
      $ul[$idx]['attributes']['class'][] = 'nav__li--leaf';
    }
    else {
      $ul[$idx]['attributes']['class'][] = 'nav__li--parent';
      _wt_cms_menuAddAttributes($ul[$idx]['below'], $level + 1, $modifier, $attachColorbox);
    }
  }
}
